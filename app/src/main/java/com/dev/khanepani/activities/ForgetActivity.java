package com.dev.khanepani.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.dev.khanepani.R;
import com.google.android.material.textfield.TextInputEditText;

public class ForgetActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private TextInputEditText inputPhoneNumber;
    private TextView sendOtpBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget);

        setupToolbar();
        setupViews();

        init();
    }

    private void init() {
        sendOtpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                TODO: network send OTP number for registration
                startActivity(new Intent(ForgetActivity.this, OTPRegisterActivity.class));
            }
        });
    }


    private void setupViews() {
        inputPhoneNumber = findViewById(R.id.inputPhoneNumber);
        sendOtpBtn = findViewById(R.id.sendOtpBtn);
    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("Forgot Password?");
    }
}
