package com.dev.khanepani.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.widget.Toolbar;

import com.dev.khanepani.R;
import com.google.android.material.textfield.TextInputEditText;

public class OTPRegisterActivity extends BaseActivity {

    private Toolbar toolbar;
    private TextInputEditText inputOTPNumber, inputPassword, inputConfirmPassword;
    private TextView btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_otpregister);
        setContentView(R.layout.activity_otpregister);

        setupToolbar();
        setToolbarTitle("Register");
        setupViews();
        init();

    }

    private void init() {
        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                TODO: network registration
                finish();
            }
        });
    }

    private void setupViews() {
        inputOTPNumber = findViewById(R.id.inputOTPNumber);
        inputPassword = findViewById(R.id.inputPassword);
        inputConfirmPassword = findViewById(R.id.inputConfirmPassword);
        btnRegister = findViewById(R.id.btnRegister);

    }

    private void setupToolbar() {
        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void setToolbarTitle(String title){
        toolbar.setTitle(title);
    }
}
